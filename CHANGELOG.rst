Changelog
===========
All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased 0.0.1] - 2019-03-31
-------------------------------
Added
+++++
- prime for documents [ok]
- prime for UML Model [pendind]

[1.0.0] - xxxx-xx-xx
-------------------------------
Added
+++++
- yet to be planned
