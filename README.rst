Vitollino
=========
A game engine to use with project SuperPython.
Runs on Brython in the browser and uses DOM based elements

Look how easy it is to use:
::

    from vitollino import Jogo
    jogo = Jogo(800, 600)
    # Get your stuff done
    jogo.cena("http://imgur.com/xxx.png").vai()

    jogo.vai()

Features
--------

- Simple Gaming
- run in web

Installation
------------

Install vitollino by running:

    pip3 install vitollino

Contribute
----------

- Issue Tracker: https://gitlab.com/cetoli/vitollino/issues
- Source Code: https://gitlab.com/cetoli/vitollino

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: project@google-groups.com

License
-------

The project is licensed under the GNU GPL-3 license.
